CREATE TABLE IF NOT EXISTS automaton.model (
	label_id text NOT NULL,
	model_data jsonb NOT NULL DEFAULT '{}'::jsonb
);

CREATE TABLE IF NOT EXISTS automaton.instance (
	automaton_label_id text NOT NULL
        current_state jsonb NOT NULL DEFAULT '{}'::jsonb
);

CREATE TABLE IF NOT EXISTS automaton.states (
	state_label_id text NOT NULL
        state_data jsonb NOT NULL DEFAULT '{}'::jsonb
);




