CREATE OR REPLACE FUNCTION automaton.get_models()
 RETURNS TABLE(data jsonb)
 LANGUAGE sql
AS $function$
		select label_id from automaton.model;
$function$
;

CREATE OR REPLACE FUNCTION automaton.update_model(p_label text, p_data text)
 RETURNS void
 LANGUAGE sql
AS $function$
                update automaton.model set model_data = p_data::JSONB
                where label_id = p_label
$function$
;

CREATE OR REPLACE FUNCTION auth.get_user_data(p_username text)
 RETURNS TABLE(data jsonb)
 LANGUAGE sql
AS $function$
		select user_data from auth.users where username = p_username
$function$
;

CREATE OR REPLACE FUNCTION auth.create_user(p_username text, p_email text, p_user_data text)
 RETURNS void
 LANGUAGE sql
AS $function$
		insert into auth.users (username, user_email, user_data) 
		values (p_username, p_email, p_user_data::JSONB)
$function$
;

CREATE OR REPLACE FUNCTION auth.update_user(p_username text, p_user_data text)
 RETURNS void
 LANGUAGE sql
AS $function$
                update auth.users set user_data = p_user_data::JSONB
                where username = p_username
$function$
;



