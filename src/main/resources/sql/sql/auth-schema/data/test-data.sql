insert into automaton.model (label_id, model_id) 
VALUES ('NAVETTA-LOGISTICA-1L','{"description":"Descrizione Navetta","startLoc":"0","endLoc":"1", arrival_time:1595263481, "current_pos":"9", "board_load":[{"load_type":"Descrizione prodotto trasportato", load_amount}], parameters:"{}"}'::jsonb);

insert into automaton.instance (automaton_label_id, current_state) 
VALUES ('NAVETTA-LOGISTICA-1L','{}'::jsonb);

insert into automaton.states (state_label_id, state_data) 
VALUES ('ON_CHARGE','{"state":"ON_CHARGE"}'::jsonb);
insert into automaton.states (state_label_id, state_data) 
VALUES ('WAITING_UNLOAD','{"state":"WAITING_UNLOAD"}'::jsonb);
insert into automaton.states (state_label_id, state_data) 
VALUES ('IN_TRAVEL_UNLOAD','{"state":"IN_TRAVEL_UNLOAD"}'::jsonb);
VALUES ('FAULT','{"state":"FAULT"}'::jsonb);
