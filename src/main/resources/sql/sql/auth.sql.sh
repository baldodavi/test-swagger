# Le operazioni sono in ordine di esecuzione, vengono scritte solo le strutture e non i dati per non incorrere in errori
# e mantenere i dati di produzione
psql "user=suma password=suma_password host=localhost port=50989 dbname=suma_db" -f ./auth-schema/schema.sql
psql "user=suma password=suma_password host=localhost port=50989 dbname=suma_db" -f ./auth-schema/ddl/tables.sql
psql "user=suma password=suma_password host=localhost port=50989 dbname=suma_db" -f ./auth-schema/ddl/constraints.sql
psql "user=suma password=suma_password host=localhost port=50989 dbname=suma_db" -f ./auth-schema/ddl/permissions.sql
psql "user=suma password=suma_password host=localhost port=50989 dbname=suma_db" -f ./auth-schema/stored-code/functions.sql
psql "user=suma password=suma_password host=localhost port=50989 dbname=suma_db" -f ./auth-schema/data/reference-data.sql
