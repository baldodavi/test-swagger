/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.model;

/**
 *
 * @author ebolo
 */
public class AutomtonParameter {

    private String parameterType;
    private String parameterValue;
    private String parameterDesc;

    public AutomtonParameter(String parameterType, String parameterValue, String parameterDesc) {
        this.parameterType = parameterType;
        this.parameterValue = parameterValue;
        this.parameterDesc = parameterDesc;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getParameterDesc() {
        return parameterDesc;
    }

    public void setParameterDesc(String parameterDesc) {
        this.parameterDesc = parameterDesc;
    }

}
