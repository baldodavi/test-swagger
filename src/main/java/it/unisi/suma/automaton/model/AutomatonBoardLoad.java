/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.model;

/**
 *
 * @author ebolo
 */
public class AutomatonBoardLoad {

  private String loadType;
  private long loadAmount;

  public AutomatonBoardLoad(String loadType, long loadAmount) {
    this.loadType = loadType;
    this.loadAmount = loadAmount;
  }

  public String getLoadType() {
    return loadType;
  }

  public void setLoadType(String loadType) {
    this.loadType = loadType;
  }

  public long getLoadAmount() {
    return loadAmount;
  }

  public void setLoadAmount(long loadAmount) {
    this.loadAmount = loadAmount;
  }

}
