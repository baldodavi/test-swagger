/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.model;

/**
 *
 * @author David
 */
public class AutomatonState {

  public final static String MAIN_STATE_ON_CHARGE = "In Ricarica";
  public final static String MAIN_STATE_WAITING = "In attesa";
  public final static String MAIN_DESC_WAITING_UNLOAD = "di scarico";
  public final static String MAIN_DESC_WAITING_LOAD = "di carico";
  public final static String MAIN_DESC_WAITING_CALL = "di chiamata";
  public final static String MAIN_STATE_IN_TRAVEL = "In viaggio";
  public final static String MAIN_DESC_IN_TRAVEL_UNLOAD = "verso lo scaricamento";
  public final static String MAIN_DESC_IN_TRAVEL_LOAD = "verso il caricamento";
  public final static String MAIN_DESC_IN_TRAVEL_CALL = "verso la ricarica batterie";
  public final static String MAIN_STATE_FAULT = "In Errore";
  public final static String MAIN_DESC_FAULT_UNKNOWN = "per motivazione sconosciuta";

  private String mainState;
  private String description;
  private String enumState;

  public AutomatonState(String mainState, String description) {
    this.enumState = getEnumState(mainState);
    this.mainState = mainState;
    this.description = description;
  }

  public String getMainState() {
    return mainState;
  }

  public String getDescription() {
    return description;
  }

  public String getEnumState(String mainState) {
    switch (mainState) {
      case MAIN_STATE_ON_CHARGE:
        return "MAIN_STATE_ON_CHARGE";
      case MAIN_DESC_WAITING_UNLOAD:
        return "MAIN_DESC_WAITING_UNLOAD";
      case MAIN_DESC_WAITING_LOAD:
        return "MAIN_DESC_WAITING_LOAD";
      case MAIN_DESC_WAITING_CALL:
        return "MAIN_DESC_WAITING_CALL";
      case MAIN_STATE_IN_TRAVEL:
        return "MAIN_STATE_IN_TRAVEL";
      case MAIN_DESC_IN_TRAVEL_UNLOAD:
        return "MAIN_DESC_IN_TRAVEL_UNLOAD";
      case MAIN_DESC_IN_TRAVEL_LOAD:
        return "MAIN_DESC_IN_TRAVEL_LOAD";
      case MAIN_DESC_IN_TRAVEL_CALL:
        return "MAIN_DESC_IN_TRAVEL_CALL";
      case MAIN_STATE_FAULT:
        return "MAIN_STATE_FAULT";
      default:
        return "MAIN_DESC_FAULT_UNKNOWN";
    }
  }

}
