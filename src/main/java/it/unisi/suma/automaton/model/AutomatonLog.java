/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.model;

/**
 *
 * @author David
 */
public class AutomatonLog {

    private String log;
    private String date;
    private String automatonId;

    public AutomatonLog(String log, String date, String automatonId) {
        this.log = log;
        this.date = date;
        this.automatonId = automatonId;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAutomatonId() {
        return automatonId;
    }

    public void setAutomatonId(String automatonId) {
        this.automatonId = automatonId;
    }

}
