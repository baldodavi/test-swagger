/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.model;

import java.util.ArrayList;

/**
 *
 * @author ebolo
 */
public class Automaton {

    private String labelId;
    private String desc;
    private AutomatonPosition startLoc;
    private AutomatonPosition endLoc;
    private long arrivalTime;
    private AutomatonPosition currentPos;
    private ArrayList<AutomatonBoardLoad> boardLoad;
    private AutomatonState currentState;
    private ArrayList<AutomtonParameter> parameters;

    public Automaton(String labelId, String desc, AutomatonPosition startLoc, AutomatonPosition endLoc, long arrivalTime, AutomatonPosition currentPos,
            ArrayList<AutomatonBoardLoad> boardLoad, AutomatonState currentState, ArrayList<AutomtonParameter> parameters) {
        this.labelId = labelId;
        this.desc = desc;
        this.startLoc = startLoc;
        this.endLoc = endLoc;
        this.arrivalTime = arrivalTime;
        this.currentPos = currentPos;
        this.boardLoad = boardLoad;
        this.currentState = currentState;
        this.parameters = parameters;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public ArrayList<AutomatonBoardLoad> getBoardLoad() {
        return boardLoad;
    }

    public void setBoardLoad(ArrayList<AutomatonBoardLoad> boardLoad) {
        this.boardLoad = boardLoad;
    }

    public AutomatonState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(AutomatonState currentState) {
        this.currentState = currentState;
    }

    public ArrayList<AutomtonParameter> getParameters() {
        return parameters;
    }

    public void setParameters(ArrayList<AutomtonParameter> parameters) {
        this.parameters = parameters;
    }

    public AutomatonPosition getStartLoc() {
        return startLoc;
    }

    public void setStartLoc(AutomatonPosition startLoc) {
        this.startLoc = startLoc;
    }

    public AutomatonPosition getEndLoc() {
        return endLoc;
    }

    public void setEndLoc(AutomatonPosition endLoc) {
        this.endLoc = endLoc;
    }

    public AutomatonPosition getCurrentPos() {
        return currentPos;
    }

    public void setCurrentPos(AutomatonPosition currentPos) {
        this.currentPos = currentPos;
    }

}
