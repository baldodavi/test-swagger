/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.errors;

/**
 *
 * @author ebolo
 */
public class NavettaNotFoundError {

  public static final String ERROR_CODE = "#4";
  public static final String ERROR_BACKEND_STRING = "La Navetta cercata non è esistente";
  public static final String ERROR_FRONTEND_STRING = "La Navetta cercata non è esistente";

  public NavettaNotFoundError() {

  }
}
