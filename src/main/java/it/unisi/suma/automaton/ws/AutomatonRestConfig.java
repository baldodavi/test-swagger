/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.ws;

import io.swagger.v3.jaxrs2.integration.JaxrsOpenApiContextBuilder;
import io.swagger.v3.oas.integration.OpenApiConfigurationException;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.ServletConfig;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;

/**
 *
 * @author ebolo
 */
@ApplicationPath("/rest")
public class AutomatonRestConfig extends Application {

  public AutomatonRestConfig(@Context ServletConfig servletConfig) {

    OpenAPI oas = new OpenAPI();
    Info info = new Info()
        .title("Automaton OpenAPI")
        .description("This is a sample server Petstore server.  You can find out more about Swagger "
            + "at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, "
            + "you can use the api key `special-key` to test the authorization filters.")
        .termsOfService("http://swagger.io/terms/")
        .contact(new Contact()
            .email("test@test.com"));

    oas.info(info);

    // Uncomment this to add server to page
    /*String url = "/suma-automaton-ms";
    List<Server> servers = new ArrayList<>();
    Server server = new Server();
    server.setUrl(url);
    servers.add(server);
    oas.setServers(servers);
     */
    SwaggerConfiguration oasConfig = new SwaggerConfiguration()
        .openAPI(oas)
        .prettyPrint(true)
        .resourcePackages(Stream.of("io.swagger.sample.resource").collect(Collectors.toSet()));

    try {
      new JaxrsOpenApiContextBuilder<>()
          .servletConfig(servletConfig)
          .application(this)
          .openApiConfiguration(oasConfig)
          .buildContext(true);
    } catch (OpenApiConfigurationException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }
}
