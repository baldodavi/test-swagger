/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.ws;

import it.unisi.suma.automaton.errors.NavettaNotFoundError;
import it.unisi.suma.automaton.helper.CreateAutomatonHelper;
import it.unisi.suma.automaton.model.Automaton;
import it.unisi.suma.automaton.model.AutomatonLog;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author ebolo
 */
@Path("/automaton")
public class AutomatonRest {

  @GET
  @Path("/automatons")
  @Produces(MediaType.APPLICATION_JSON)
  public Response automatons() {
    ArrayList<Automaton> autoList = new ArrayList<>();
    autoList.add(CreateAutomatonHelper.createAutomaton(CreateAutomatonHelper.N1));
    autoList.add(CreateAutomatonHelper.createAutomaton(CreateAutomatonHelper.N2));
    autoList.add(CreateAutomatonHelper.createAutomaton(CreateAutomatonHelper.N3));
    return Response.ok(autoList).build();
  }

  @GET
  @Path("/automaton/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response automaton(@PathParam("id") String labelId) {
    if (labelId.equals(CreateAutomatonHelper.N1)
        || labelId.equals(CreateAutomatonHelper.N2)
        || labelId.equals(CreateAutomatonHelper.N3)) {

      Automaton autom = CreateAutomatonHelper.createAutomaton(labelId);
      return Response.ok(autom).build();
    } else {
      return Response.status(Response.Status.NOT_FOUND).entity(new NavettaNotFoundError()).build();
    }

  }

  @GET
  @Path("/automaton/logs/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response automatonLogs(@PathParam("id") String labelId) {
    if (labelId.equals(CreateAutomatonHelper.N1)
        || labelId.equals(CreateAutomatonHelper.N2)
        || labelId.equals(CreateAutomatonHelper.N3)) {
      List<AutomatonLog> logs = CreateAutomatonHelper.createAutomatonLogs();
      return Response.ok(logs).build();
    } else {
      return Response.status(Response.Status.NOT_FOUND).entity(new NavettaNotFoundError()).build();
    }

  }

}
