/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unisi.suma.automaton.helper;

import it.unisi.suma.automaton.model.Automaton;
import it.unisi.suma.automaton.model.AutomatonBoardLoad;
import it.unisi.suma.automaton.model.AutomatonLog;
import it.unisi.suma.automaton.model.AutomatonPosition;
import it.unisi.suma.automaton.model.AutomatonState;
import it.unisi.suma.automaton.model.AutomtonParameter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David
 */
public class CreateAutomatonHelper {

  public final static String N1 = "NAVETTA_DEMO_L1";
  public final static String N2 = "NAVETTA_DEMO_L1_2";
  public final static String N3 = "NAVETTA_DEMO_L3";

  public static Automaton createAutomaton(String type) {
    switch (type) {
      case N1:
        return getFirstAutomaton();
      case N2:
        return getSecondAutomaton();
      case N3:
        return getThirdAutomaton();
      default:
        return getThirdAutomaton();
    }
  }

  private static Automaton getFirstAutomaton() {
    return new Automaton(
        "NAVETTA_DEMO_L1",
        "Descrizione Navetta 1",
        new AutomatonPosition(0, 0),
        new AutomatonPosition(10, 10),
        1595263481,
        new AutomatonPosition(5, 5),
        new ArrayList<AutomatonBoardLoad>() {
      {
        add(new AutomatonBoardLoad("Carico 1", 2));
        add(new AutomatonBoardLoad("Carico 2", 3));
      }
    },
        new AutomatonState(AutomatonState.MAIN_STATE_ON_CHARGE, ""),
        new ArrayList<AutomtonParameter>() {
      {
        add(new AutomtonParameter("P1", "V1", "Desc 1"));
        add(new AutomtonParameter("P2", "V2", "Desc 2"));
      }
    }
    );
  }

  private static Automaton getSecondAutomaton() {
    return new Automaton(
        "NAVETTA_DEMO_L1_2",
        "Descrizione Navetta 2",
        new AutomatonPosition(1, 1),
        new AutomatonPosition(22, 22),
        1595263485,
        new AutomatonPosition(7, 4),
        new ArrayList<AutomatonBoardLoad>() {
      {
        add(new AutomatonBoardLoad("Carico 3", 2));
        add(new AutomatonBoardLoad("Carico 4", 3));
      }
    },
        new AutomatonState(AutomatonState.MAIN_STATE_FAULT, AutomatonState.MAIN_DESC_FAULT_UNKNOWN),
        new ArrayList<AutomtonParameter>() {
      {
        add(new AutomtonParameter("P1", "V1", "Desc 1"));
        add(new AutomtonParameter("P2", "V2", "Desc 2"));
      }
    }
    );
  }

  private static Automaton getThirdAutomaton() {
    return new Automaton(
        "NAVETTA_DEMO_L3",
        "Descrizione Navetta 3",
        new AutomatonPosition(3, 3),
        new AutomatonPosition(13, 13),
        1595263481,
        new AutomatonPosition(8, 5),
        new ArrayList<AutomatonBoardLoad>() {
      {
        add(new AutomatonBoardLoad("Carico 1", 5));
        add(new AutomatonBoardLoad("Carico 2", 8));
      }
    },
        new AutomatonState(AutomatonState.MAIN_STATE_IN_TRAVEL, AutomatonState.MAIN_DESC_IN_TRAVEL_LOAD),
        new ArrayList<AutomtonParameter>() {
      {
        add(new AutomtonParameter("P1", "V1", "Desc 1"));
        add(new AutomtonParameter("P2", "V2", "Desc 2"));
      }
    }
    );
  }

  public static List<AutomatonLog> createAutomatonLogs() {
    ArrayList<AutomatonLog> result = new ArrayList<>();
    result.add(new AutomatonLog("Log1", "1595263481", "NAVETTA_DEMO_L3"));
    result.add(new AutomatonLog("Log2", "1595263482", "NAVETTA_DEMO_L3"));
    result.add(new AutomatonLog("Log3", "1595263483", "NAVETTA_DEMO_L3"));
    result.add(new AutomatonLog("Log4", "1595263484", "NAVETTA_DEMO_L3"));
    result.add(new AutomatonLog("Log5", "1595263485", "NAVETTA_DEMO_L3"));
    result.add(new AutomatonLog("Log6", "1595263486", "NAVETTA_DEMO_L3"));

    return result;
  }

}
